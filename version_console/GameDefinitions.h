#ifndef GAME_DEFINITONS_H
#define GAME_DEFINITONS_H

enum class CardType  //enum 
{
    None,
    Guard = 1,
    Priest,
    Baron,
    Handmaiden,
    Prince,
    King,
    Countess,
    Princess,
};

typedef unsigned char Id;


#endif // GAME_DEFINITONS_H
