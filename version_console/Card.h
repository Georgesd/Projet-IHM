#ifndef CARD_H
#define CARD_H

#include <string>
#include "GameDefinitions.h"

/**
    Renvoie une chaine de caractères du type de la carte

    @param CardType
    @return string
*/
std::string CardTypeToString(CardType);

/**
    Renvoie le type de la carte depuis une chaine de caractères

    @param string&
    @return CardType
*/
CardType CardTypeFromString(const std::string&);

/**
    Renvoie l'effet de la carte

    @param Cardtype
    @return string
*/
std::string GetCardStrength(CardType);

/**
    Renvoie la valeur numérique de la carte

    @param CardType
    @return int
*/
int GetCardValue(CardType cardType);

/**
    Renvoie le nombre de carte d'un type

    @param CardType
    @return int
*/
int GetNumbCard(CardType cardType);

struct Card
{
    typedef unsigned char Strength;

    CardType type ;      //1 2 3.. guard baron handmaid etc
    Strength strength;  // which one is the stronger card
};

#endif // CARD_H
