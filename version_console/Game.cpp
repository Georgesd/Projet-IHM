#include "Game.h"
#include <algorithm>


int Game::NbPlayerProtected()
{
    int i;
	for(auto j : vectorPlayers)
    {
        if(j->GetPlayerProtection() == true)
        {
            i++;
        }
    }
    return i;
}


bool Game::AllProtected(Joueur * moi)
{
	for(auto j : vectorPlayers)
    {
        if(j->GetName() == moi->GetName())
        {
            
        }
        else
        {
            if(j->GetPlayerProtection() == false && j->isDead== false)
            {
               return false;
            }
        }   
    }
    return true;  
}

void Game::PrintPlayers()
{
    for (auto j : vectorPlayers)
    j->PrintName();
}


void Game::PlayersShowHands()
{
    for (auto j : vectorPlayers)
    {
        if(j->isDead == false)
        {
            j->PrintName();
            j->PrintHand(); 
        }
    }
}

//nPlayer vector array of all the Players(Joueur.cpp)
void Game::InitPlayers(int nbPlayers)
{
    std::string in;
    std::string player;
    while(nbPlayers < 2 || nbPlayers > 4)
    {
        std::cout << "Choisissez un nombre entre 2 et 4 : " << std::endl;
        getline(std::cin, in); 
        nbPlayers = std::stoi(in);
    }
    std::string input = "";
    vectorPlayers.clear();
    int num = 1;
    for(int i = 0 ; i < nbPlayers; i++)
    {
        std::cout <<"Quel type de joueur ? h pour humain, d pour adversaire difficile, m pour adversaire moyen ou f pour adversaire facile " << std::endl;
        getline(std::cin, player);
        if (player == "h")
        {
            printf("Entrez votre nom : "); //change here according to different input modes
            getline(std::cin, input);    //====
            Joueur* joueur = new Joueur(input);
            vectorPlayers.insert(vectorPlayers.begin(), 1, joueur); //array players with names from ->//input method

        }
        if (player == "d")
        {
            std::string n;
            if(num == 1)
                n = "Jack";
            if(num == 2)
                 n = "Alice";
            if(num == 3)
                n = "Tom";
            if(num == 4)
                 n = "Paul";
            Terminator* term = new Terminator(n);
            vectorPlayers.insert(vectorPlayers.begin(), 1, term);
            vectorBot.insert(vectorBot.begin(), 1, term);

            num++;
        }
        if (player == "f")
        {
            std::string n;
            if(num == 1)
                n = "Jack";
            if(num == 2)
                 n = "Alice";
            if(num == 3)
                n = "Tom";
            if(num == 4)
                 n = "Paul";
            Bot_Random* Bot = new Bot_Random(n);
            vectorPlayers.insert(vectorPlayers.begin(), 1,Bot);
            
            num++;
        }
        if (player == "m")
        {
            std::string n;
            if(num == 1)
                n = "Jack";
            if(num == 2)
                 n = "Alice";
            if(num == 3)
                n = "Tom";
            if(num == 4)
                 n = "Paul";
            InfSup_Bot* inf_bot = new InfSup_Bot(n);
            vectorPlayers.insert(vectorPlayers.begin(), 1, inf_bot);
            vectorBot.insert(vectorBot.begin(), 1, inf_bot);

            num++;
        }

    }
    std::vector<Card> c;
    for(unsigned int i = 0; i<vectorPlayers.size(); i++)
    {
        PlayedCards p = {vectorPlayers.at(i), c};
        playedCard.insert(playedCard.begin(),1,p);
    }
    for(unsigned int k=0; k<vectorBot.size(); k++)
        for(unsigned int i = 0; i<vectorPlayers.size(); i++)
        {
            PlayedCards p = {vectorPlayers.at(i), c};
            vectorBot.at(k)->playedCard.push_back(p);
        }
    numbreOfPlayers = nbPlayers;
}

int Game::PlayersAlive() const
{
    int nbAlive=0;
    for(Joueur* j : vectorPlayers)
    {
      if(j->isDead == false)
      {
        nbAlive ++ ;  
      }
    }
    return nbAlive;
}


int Game::GetPlayerPosition(Joueur* j)//return position inside the array
{
	int i = 0;
   	while (j->GetName() != vectorPlayers.at(i)->GetName())
	   i++;
	return i;

}   

Joueur* Game::TakePlayer(int n)
{
    if(n > PlayersAlive()) //if n bigger than players available in the vector
    {
        exit (EXIT_FAILURE);
    }
    Joueur* j= vectorPlayers.at(n-1);
    return j;
}


void Game::ErasePlayer(int n)
{
    vectorPlayers.erase(vectorPlayers.begin() + n);
}


Card Game::CardComppare(const Card& a,const Card& b)
{
    if(a.type > b.type)
    {
        return b;
    }
  return a;
}

Card Game::GetDefausse()
{
    return defausse;
}

void Game::PrintDefausse()
{
    std::string card = CardTypeToString(defausse.type);
    std::cout << card << std::endl;
}

void Game::CardEffectCheck(const Card& c, Deck * deck ,Joueur * j,int pos)// int pos current player position
{
    bool present = false;
	std::string input;
    if( CardTypeToString(c.type) == "Guard")
    {
        if(AllProtected(j))
        {}
        else
        { 
            do
            {
                input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                while (j->GetName() == input) 
                {
                    input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                }
                std::cout << input << std::endl;
                for (auto p : vectorPlayers)
                {
                    if(p->GetName() == input && p->GetPlayerProtection() == false && p->isDead == false )
                    {
                        int i = GetPlayerPosition(p);
                        Card input = vectorPlayers.at(pos)->DevinerCarte();
                        std::cout << "Proposition de carte : " << CardTypeToString(input.type) << std::endl;
                        Card c = p->TakeCardTop(); 
                        if( CardTypeToString(input.type) ==  CardTypeToString(c.type) )
                        {
                            std::cout << "Vous avez trouvé ! " <<std::endl;
                            vectorPlayers.at(i)->isDead = true;
                            playedCard.at(i).cards.push_back(c);
                            for (unsigned int k = 0; k<vectorBot.size(); k++)
                                vectorBot.at(k)->playedCard.at(i).cards.push_back(c);
                        }
                        else
                        {
                            std::cout << "Non désolé..."<<std::endl;
                        }
                        present = true;
                    }
                }
            } while (present == false);  
        }
    }

    if ( CardTypeToString(c.type) == "Princess" )
    {
        std::cout << "Vous avez perdu"  <<std::endl ;
        vectorPlayers.at(pos)->isDead = true;
        std::cout << PlayersAlive() <<std::endl;
    }


    if( CardTypeToString(c.type) == "Baron" )
    {
        if(AllProtected(j))
        {}
        else
        {
            do
            {
                input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                while (j->GetName() == input) 
                {
                    input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                }
                for (auto p : vectorPlayers)
                {
                    if(p->GetName() == input && p->GetPlayerProtection() == false && p->isDead == false )
                    {
                        int index = GetPlayerPosition(p);
                        Card otherCard = vectorPlayers.at(index)->TakeCardTop();
                        std::cout << CardTypeToString(otherCard.type) << std::endl;

                        Card myCard =  vectorPlayers.at(pos)->TakeCardTop();
                        std::cout << CardTypeToString(myCard.type) << std::endl;

                        if( otherCard.type > myCard.type)//my caard is lower
                        {    
                            std::cout << "Vous avez perdu" <<std::endl ;
                            vectorPlayers.at(pos)->isDead = true;
                            playedCard.at(pos).cards.push_back(myCard);
                            for (unsigned int k = 0; k<vectorBot.size(); k++)
                                vectorBot.at(k)->playedCard.at(pos).cards.push_back(myCard);
                        }
                        else if(otherCard.type < myCard.type)
                        {
                            std::cout << "Vous avez gagné le duel !"  <<std::endl ;
                            vectorPlayers.at(index)->isDead = true;
                            playedCard.at(index).cards.push_back(otherCard);
                            for (unsigned int k = 0; k<vectorBot.size(); k++)
                                vectorBot.at(k)->playedCard.at(index).cards.push_back(otherCard);
                        }
                        else{/*do nothing*/}
                        present = true;
                    }
                }
            } while (present == false );
        }		
    }

    if( CardTypeToString(c.type) == "Prince" )
    {
        if(AllProtected(j))
        {
            std::cout << "Tous les autres joueurs sont protégés, vous n'avez pas le choix" << std::endl;
            Card discard = vectorPlayers.at(pos)->TakeCardTop();
            if(CardTypeToString(discard.type) == "Princess")
            {
                vectorPlayers.at(pos)->isDead = true;
                std::cout <<"Vous avez perdu" << std::endl;
            }
            else
            {
                Card newcard;
                vectorPlayers.at(pos)->PlayACard(discard);
                if (deck->SizeDeck() == 0)
                    newcard = defausse;
                else
                    newcard = deck->PickCard();
                vectorPlayers.at(pos)->AddCard(newcard);
            }
        }
        else
        {
            do
            {
                input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                for (Joueur* p : vectorPlayers)
                {
                    if (p->GetName() == input && p->GetPlayerProtection() == false && p->isDead == false)
                    {
                        int i= GetPlayerPosition(p);
                        Card discard = vectorPlayers.at(i)->TakeCardTop();
                        if(CardTypeToString(discard.type) == "Princess")
                        {
                            vectorPlayers.at(i)->isDead = true;
                            std::cout <<"Vous avez perdu" << std::endl;
                        }
                        else
                        {
                            vectorPlayers.at(i)->PlayACard(discard);
                            Card newcard = deck->PickCard();
                            vectorPlayers.at(i)->AddCard(newcard);
                        }
                        present = true;
                    }
                }
            } while (present == false);	
        }	
    }

    if( CardTypeToString(c.type) == "Handmaiden" )
    {
        j->ProtecdPlayer();
        std::cout<< "Vous êtes protégés pour ce tour" <<std::endl;
    }

    if( CardTypeToString(c.type) == "King" )
    {
        if(AllProtected(j))
        {}
        else
        {
            do
            {
                input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                while (j->GetName() == input) 
                {
                    input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                }
                for (auto p : vectorPlayers)
                {
                    if(p->GetName() == input && p->GetPlayerProtection() == false && p->isDead == false)
                    {
                        int i = GetPlayerPosition(p);
                        std::swap(vectorPlayers[i]->playerDeck[0],vectorPlayers[pos]->playerDeck[0]);     // /!\ playerDeck is private
                        present = true;
                    }
                }
            } while (present == false);
        }
    }

    if( CardTypeToString(c.type) == "Priest" )
    {
         if(AllProtected(j))
        {}
        else
        {
            do
            {
                input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                while (j->GetName() == input) 
                {
                    input = j->ChoisirJoueur(vectorPlayers, vectorPlayers.size());
                }
                for (auto p : vectorPlayers)
                {
                    if(p->GetName() == input && p->GetPlayerProtection() == false && p->isDead == false)
                    {
                        p->PrintHand();
                        present = true;
                    }
                }
            } while (present == false);		
        }
    }
}


void Game::PlayRound(Deck deck)
{
    Card discard;
    Card c;
    bool playerExist;
    //initialize player vector
   //  InitPlayers(nbPlayers);
    //PickCard take one card and remove it from the deck //GAME RULE1
    defausse = deck.PickCard();
    //PrintDefausse();
    
    //si 2 joueurs => pioche trois cartes et les montre a tous le monde //GAME RULE2
    if(PlayersAlive() == 2)                           
    {
        std::cout << "CARTES RETIREES"<< std::endl;
        discard = deck.PickCard();
        std::cout << CardTypeToString(discard.type) << std::endl;
        discard = deck.PickCard();
        std::cout << CardTypeToString(discard.type) << std::endl;
        discard = deck.PickCard();
        std::cout << CardTypeToString(discard.type) << std::endl;
        std::cout << "================" << std::endl;
    }

    //EVERYONE RECIEVE ONE CARD //GAME RULE3
    for(int i = 0; i <numbreOfPlayers; i++)
    {
        auto pickedCard1 = deck.PickCard(); 
        vectorPlayers.at(i)->AddCard(pickedCard1);
    }



    while(deck.SizeDeck() > 0)  
    {
        //CHECK MORE THAN  PLAYER AVAILABLE
       if( PlayersAlive() <= 1)
       {
           break;
       }

        for(int i =0  ; i <numbreOfPlayers; i++)
        {
            if(vectorPlayers.at(i)->isDead == false)
            {
            //GameRULE protection last only 1 tour
            if(vectorPlayers.at(i)->GetPlayerProtection() == true)
            {
                std::cout<< "Vous n'êtes plus protégé" <<std::endl;
                vectorPlayers.at(i)->RemovePlayerProtection(); 
            }

            vectorPlayers.at(i)->PrintName();
            Card inHand = vectorPlayers.at(i)->TakeCardTop();
            Card pick = deck.PickCard();
            if(CardTypeToString(pick.type) == "Countess" && (CardTypeToString(inHand.type) =="Prince" || CardTypeToString(inHand.type)=="King"))
            {
                vectorPlayers.at(i)->AddCard(pick);
                if (vectorPlayers.at(i)->GetName() != "Jack" && vectorPlayers.at(i)->GetName() != "Alice" && vectorPlayers.at(i)->GetName() != "Tom" )
                {
                    std::cout << "Vos cartes : " << std::endl;
                    vectorPlayers.at(i)->PrintHand();
                    std::cout <<"Vous n'avez pas le choix" << std::endl;
                }
                vectorPlayers.at(i)->PlayACard(pick);
                playedCard.at(i).cards.push_back(pick);
                for (unsigned int k = 0; k<vectorBot.size(); k++)
                    vectorBot.at(k)->playedCard.at(i).cards.push_back(pick);
            }
            else
            {
                if (CardTypeToString(inHand.type) == "Countess" && (CardTypeToString(pick.type) =="Prince" || CardTypeToString(pick.type)=="King"))
                {
                    vectorPlayers.at(i)->AddCard(pick);
                if (vectorPlayers.at(i)->GetName() != "Jack" && vectorPlayers.at(i)->GetName() != "Alice" && vectorPlayers.at(i)->GetName() != "Tom" )
                {    
                    std::cout << "Vos cartes : " << std::endl;
                    vectorPlayers.at(i)->PrintHand();
                    std::cout <<"Vous n'avez pas le choix" << std::endl;
                }
                    vectorPlayers.at(i)->PlayACard(inHand);
                    playedCard.at(i).cards.push_back(inHand);
                    for (unsigned int k = 0; k<vectorBot.size(); k++)
                        vectorBot.at(k)->playedCard.at(i).cards.push_back(inHand);
                }
                else
                {
                    vectorPlayers.at(i)->AddCard(pick);
                    if (vectorPlayers.at(i)->GetName() != "Jack" && vectorPlayers.at(i)->GetName() != "Alice" && vectorPlayers.at(i)->GetName() != "Tom" )
                    {
                        std::cout << "Vos cartes : " << std::endl;
                        vectorPlayers.at(i)->PrintHand();
                    }

                    do
                    {
                        c = vectorPlayers.at(i)->ChoisirCarte();
                        playedCard.at(i).cards.push_back(c);
                        for (unsigned int k = 0; k<vectorBot.size(); k++)
                        {
                            vectorBot.at(k)->playedCard.at(i).cards.push_back(c);
                        }
                        playerExist = vectorPlayers.at(i)->PlayACard(c);
                    } while( playerExist == false);
                    CardEffectCheck(c, &deck , vectorPlayers.at(i) ,i);
                    std::cout <<"" <<std::endl;
                    std::cout << "######## CARTES DEJA JOUEES ########" <<std::endl;
                    for (unsigned int k = 0; k< playedCard.at(i).cards.size(); k++)
                    {
                        std::string s = CardTypeToString(playedCard.at(i).cards.at(k).type);
                        std::cout << s << std::endl;
                    }
                    std::cout << "#####################################" <<std::endl;
                }
            }

            if( PlayersAlive() <= 1)//check if more than 1 player if not exit
            {
                break;
            }
            std::cout <<"" <<std::endl;
            std::cout << "===============================" <<std::endl;
            }
            if (deck.SizeDeck() == 0)
            {break;}

            std::cout <<""<<std::endl;
            std::cout <<""<<std::endl;
        }    
    }
    //ALL PLAYERS SHOW HANDS
    PlayersShowHands();//testonly
    if (PlayersAlive() <= 1)
    {
        for(unsigned int i = 0 ; i < vectorPlayers.size() ; i++ )
        {
            
            if(vectorPlayers.at(i)->isDead == false)
            {
                vectorPlayers.at(i)->WinAPoint();
                std::cout << "Le gagnant de cette manche est : " << std::endl;
                vectorPlayers.at(i)->PrintName();
                std::cout <<vectorPlayers.at(i)->GetNbPoints() << " point(s)" << std::endl;
            }
        }
    }
    else//PlayerAlive > 1
    {
        int winner=0;
        Card win;
        win.type = CardType::None;
        for(unsigned int i = 0 ; i < vectorPlayers.size() ; i++ )
        {   
            if(vectorPlayers.at(i)->isDead == false )
            {
                if(vectorPlayers.at(i)->TakeCardTop().type > win.type)
                {
                    win.type = vectorPlayers.at(i)->TakeCardTop().type;
                    winner = i;
                }
            }
        }
        vectorPlayers.at(winner)->WinAPoint();
        std::cout << "Le gagnant de cette manche est : " << std::endl;
        vectorPlayers.at(winner)->PrintName();
        std::cout <<vectorPlayers.at(winner)->GetNbPoints() << " point(s)" << std::endl;
    }
}


void Game::PlayGame()
{
    std::string nombreJ;
    std::cout << "A combien de joueurs voulez-vous jouer ?" << std::endl;
    getline(std::cin, nombreJ);
    int nb = std::stoi(nombreJ);
    InitPlayers(nb);
    int i = -1;
    while (i== -1)
    {
        Deck deck;
        deck.InitDeck();
        PlayRound(deck);
        Between2Rounds();
        i = Winner(nb);
    }
    std::cout << "Le gagnant est ";
    vectorPlayers.at(i)->PrintName();
    std::cout << "Le résultat final est : " << std::endl;
    for(auto j : vectorPlayers)
    {
        j->PrintName();
        std::cout <<vectorPlayers.at(GetPlayerPosition(j))->GetNbPoints() << " point(s)" << std::endl;
    }

}

void Game::Between2Rounds()
{
    std::cout <<"" <<std::endl;
    std::cout <<"" <<std::endl;
    for(auto j : vectorPlayers)
    {
        int pos = GetPlayerPosition(j);
        vectorPlayers.at(pos)->isDead = false;
        vectorPlayers.at(pos)->RemovePlayerProtection();
        vectorPlayers.at(pos)->EmptyPlayerDeck();
        playedCard.at(pos).cards.clear();
    }
    for (unsigned int k = 0; k<vectorBot.size(); k++)
        for(unsigned int i; i< vectorPlayers.size(); i++)
        {
            vectorBot.at(k)->playedCard.at(i).cards.clear();
            vectorBot.at(k)->firstRound = true;
        }
    std::cout <<""<<std::endl;
    std::cout <<""<<std::endl;
    std::cout <<""<<std::endl;
}

int Game::Winner(int nbJoueur)
{
    int maxPoints;
    if (nbJoueur == 2)
        maxPoints = 7;
    if (nbJoueur == 3)
        maxPoints = 5;
    if (nbJoueur == 4)
        maxPoints = 4;
    for (int i=0; i<nbJoueur; i++)
    {
        if (vectorPlayers.at(i)->GetNbPoints() == maxPoints)
            return i;
    }
    return -1;
}


int main()
{
    //Deck deck; 
   // deck.InitDeck(); // init tas
    
    Game game;
    //std::cout << "qlkhcqdihvdshv" << std::endl;
    game.PlayGame();
    //game.PlayRound(deck,4);
  
     return 0;
}

