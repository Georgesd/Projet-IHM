#ifndef DECK_H
#define DECK_H

#include "Card.h"
#include <vector>

class Deck
{
public:

    int SizeDeck() const;
    /**
        Print tous les cartes de deck

        @param none
        @return void
    */
    void PrintDeck();

    /**
        Initialise le tas avec 16 cartes et le melange a la fin

        @param none 
        @return void
    */
    void InitDeck();

    /**
        Prend une carte du tas

        @param none
        @return Card
    */
    Card PickCard();

    /**
        Teste si le tas est vide

        @param none
        @return bool
    */
    bool IsEmpty() const;  // teste si vide
private:
    /**
        Melange le tas

        @param none
        @return void
    */
    void ShuffleDeck();
    std::vector<Card> m_deck;  // vector Card represente le tas
};

#endif // DECK_H
