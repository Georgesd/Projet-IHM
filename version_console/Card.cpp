#include "Card.h"

std::string CardTypeToString(CardType cardType)
{
    switch(cardType)
    {
        case CardType::None:       return "None";
        case CardType::Guard:      return "Guard";
        case CardType::Priest:     return "Priest";
        case CardType::Baron:      return "Baron";
        case CardType::Handmaiden: return "Handmaiden";
        case CardType::Prince:     return "Prince";
        case CardType::King:       return "King";
        case CardType::Countess:   return "Countess";
        case CardType::Princess:   return "Princess";
        default:                   return "";
    }
    return "";

}

CardType CardTypeFromString(const std::string& cardType)
{
    if(cardType == "Guard" || cardType == "1")
    {
        return CardType::Guard;
    }
    else if(cardType == "Priest" || cardType == "2")
    {
        return CardType::Priest;
    }
    else if(cardType == "Baron" || cardType == "3")
    {
        return CardType::Baron;
    }
    else if(cardType == "Handmaiden" || cardType == "4")
    {
        return CardType::Handmaiden;
    }
    else if(cardType == "Prince" || cardType == "5" )
    {
        return CardType::Prince;
    }
    else if(cardType == "King" || cardType == "6" )
    {
        return CardType::King;
    }
    else if(cardType == "Countess" || cardType == "7")
    {
        return CardType::Countess;
    }
    else if(cardType == "Princess" || cardType == "8")
    {
        return CardType::Princess;
    }
    else
        return CardType::None;
}

std::string GetCardStrength(CardType cardType)
{
    switch(cardType)
    {
        case CardType::Guard:      return "Choisissez un joueur et essayer de deviner sa carte";//
        case CardType::Priest:     return "Regardez la main d'un autre joueur";//
        case CardType::Baron:      return "Jusqu'au prochain tour, vous êtes protégé des effets des cartes des autres joueurs.";//done
        case CardType::Handmaiden: return "Comparez votre carte avec celle d'un autre joueur, celui qui a la carte avec la plus faible valeur est éliminé de la manche";//
        case CardType::Prince:     return "Choisissez un joueur (y comprisvous), celui-ci défausse la carte qu'il a en main pour en piocher une nouvelle.";//
        case CardType::King:       return "Échangez votre main avec un autre joueur de votre choix.";//
        case CardType::Countess:   return "En même temps que le King ou le Prince, alors vous devez défausser la carte de la Countess";
        case CardType::Princess:   return "Si vous défaussez cette carte,vous êtes éliminé de la manche.";//done
        default:                   return "";
    }
    return "";

}

int GetCardValue(CardType cardType)
{
    switch(cardType)
    {
        case CardType::None:       return 0;
        case CardType::Guard:      return 1;
        case CardType::Priest:     return 2;
        case CardType::Baron:      return 3;
        case CardType::Handmaiden: return 4;
        case CardType::Prince:     return 5;
        case CardType::King:       return 6;
        case CardType::Countess:   return 7;
        case CardType::Princess:   return 8;
        default:                   return 0;
    }
    return 0;

}

int GetNumbCard(CardType cardType)
{
    switch(cardType)
    {
        case CardType::None:       return 0;
        case CardType::Guard:      return 5;
        case CardType::Priest:     return 2;
        case CardType::Baron:      return 2;
        case CardType::Handmaiden: return 2;
        case CardType::Prince:     return 2;
        case CardType::King:       return 1;
        case CardType::Countess:   return 1;
        case CardType::Princess:   return 1;
        default:                   return 0;
    }
    return 0;

}



