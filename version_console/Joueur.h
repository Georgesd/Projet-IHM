#ifndef JOUEUR_H
#define JOUEUR_H
#include <iostream>
#include <string>
#include <vector>
#include "Deck.h"

class Joueur
{     
public: 
    Joueur(const std::string& name);

    /**
        Renvoie le nom du Joueur, accesseur

        @param none
        @return string
    */
    const std::string& GetName() const; //  (string& reference -> do not use a copy)

    /**
        affiche le nom du Joueur
        @param none
        @return string
    */
    void PrintName();

    /**
        Renvoie le nombre de points du Joueur, accesseur

        @param none
        @return int
    */
    int GetNbPoints() const;

    /**
        Modifie le nombre de points du Joueur, modificateur

        @param int
        @return void
    */
    void SetNbPoints(int);

    /**
        Teste si le Joueur est éliminé de la manche

        @param none
        @return bool
    */
    bool IsDead() const;

    /**
        Affiche la main du Joueur

        @param none
        @return void
    */
    void PrintHand();

    /**
        Renvoie le nombre de points du Joueur en chaine de caractère

        @param none
        @return string
    */
    std::string PrintNbPoints();

    /**
        Ajoute une carte dans la main

        @param Card&
        @return void
    */
    void AddCard(const Card& toAdd);

    /**
        Récupère la carte à la position n de la main

        @param size_t
        @return Card
    */
    Card TakeCard(size_t n);

    /**
        Récupère une carte de la main avec son nom

        @param string&
        @return Card
    */
    Card TakeCardByName(const std::string& name);

    /**
        Joue une carte de la main et retourne si ça a fonctionné ou non

        @param Card&
        @return bool
    */
    bool PlayACard(Card& c); //retire la carte c de la main du joueur et affiche le pouvoir

    /**
        Protège le Joueur en mettant l'attribut protect à true

        @param none
        @return void
    */
    void ProtecdPlayer();

    /**
        Renvoie si le Joueur est protégé ou non, accesseur

        @param none
        @return bool
    */
    bool GetPlayerProtection();

    /**
        Vide la main du Joueur

        @param none
        @return void
    */
    void EmptyPlayerDeck();

    /**
        Enlève la protection du Joueur en mettant l'attribut protect à false

        @param none
        @return void
    */
    void RemovePlayerProtection();

    /**
        Retourne la carte de la main du Joueur

        @param none
        @return Card
    */
    Card TakeCardTop();

    /**
        Ajoute un point au Joueur

        @param none
        @return void
    */
    void WinAPoint();

    /**
        Renvoie la carte à jouer

        @param none
        @return Card
    */
    virtual Card ChoisirCarte();

    /**
        Renvoie la proposition de carte à deviner pour l'effet du Guard

        @param none
        @return Card
    */
    virtual Card DevinerCarte();

    /**
        Renvoie le nom du Joueur choisi pour l'application d'un effet de carte

        @param vector<Joueur*>, int
        @return string
    */
    virtual std::string ChoisirJoueur(std::vector<Joueur*> vectorPlayer, int nbPlayers);
    
    bool isDead = false;
    std::vector<Card> playerDeck;
public:    
    bool protect = false;//The default numeric value of true is 1 and false is 0.
   //la main du joueur = ses cartes
    int nbPoints = 0;  //nombre de points du joueur
    std::string name; //nom du joueur
};


#endif //JOUEUR_H
