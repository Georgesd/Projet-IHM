#ifndef INFSUP_BOT_H
#define INFSUP_BOT_H
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include <cmath>
#include <map>
#include "Joueur.h"

struct PlayedCards
{
    Joueur* j;
    std::vector<Card> cards;
};

class InfSup_Bot: public Joueur
{
public:
	InfSup_Bot(const std::string& name); //constructeur

	/**
        Redéfinition de ChoisirCarte() de la classe Joueur

        @param none
        @return Card
    */
	Card ChoisirCarte();

	/**
        Redéfinition de ChoisirJoueur() de la classe Joueur

        @param vector<Joueur*>, int
        @return string
    */
	std::string ChoisirJoueur(std::vector<Joueur*> vectorPlayer , int nbPlayers);

	/**
        Redéfinition de DevinerCarte() de la classe Joueur

        @param none
        @return Card
    */
	Card DevinerCarte();

	/**
        Renvoie le nombre de cartes d'un type qu'il reste en jeu

        @param Card
        @return int
    */
	int CartesRestantes(Card c);
	
	std::vector<PlayedCards> playedCard;
    bool firstRound = true;
};

#endif
