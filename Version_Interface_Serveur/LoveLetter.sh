#!/bin/sh


echo "=========================================================================="
echo "Lancement Serveur"
cd new_server
make
xterm -e ./main &
cd ..
echo "=========================================================================="
echo "Lancement new_controller"
cd new_controller
make
xterm -e ./main &

echo "=========================================================================="
echo "Lancement interface"
cd ..
cd new_view/View/interface
qmake
make
./ihm
cd ..

echo "==========================================================================="
exit 0
