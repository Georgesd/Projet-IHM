#ifndef COMPUTER_H
#define COMPUTER_H

#include <QDialog>
#include <QLineEdit>
#include <QGraphicsDropShadowEffect>
#include "game.h"
#include "utils.h"

namespace Ui {
class Computer;
}

class Computer : public QDialog
{
    Q_OBJECT

public:
    explicit Computer(QWidget *parent = 0);
    ~Computer();

    QString game_mode;

    void OnControllerConnect();
    void OnControllerDisconnect();
    void SetMsgSender(MsgSender msgSender);

private slots:

    /**
        Passage à la fenêtre GAME
        Initialise la variable globale name à la valeur donnée dans LineEdit

        @param -
        @return void
    */
    void on_startButton_clicked();


    /**
        Teste s'il y a de texte dans LineEdit

        @param QString str
        @return void
    */
    void TextChanged(QString str);
    
    
    /**
       Teste si easyButton est appuyé
       Initialise la variable globale game_mode à EASY
       
        @param bool checked
        @return void
    */
    void on_easyButton_toggled(bool checked);
    
    
    /**
        Teste si mediumButton est appuyé
        Initialise la variable globale game_mode à MEDIUM
        
        @param bool checked
        @return void
    */
    void on_mediumButton_toggled(bool checked);
    
    
    /**
        Teste si hardButton est appuyé
        Initialise la variable globale game_mode à HARD
        
        @param bool checked
        @return void
    */
    void on_hardButton_toggled(bool checked);

private:
    Ui::Computer *ui;
    Game *windowGame;
    QLineEdit *textArea;
    QPushButton *startButton;

    bool m_controllerConnected = false;
    MsgSender m_msgSender = nullptr;
    void _sendMsg(const Msg&);
    void _sendInitMsg();
};

#endif // COMPUTER_H
