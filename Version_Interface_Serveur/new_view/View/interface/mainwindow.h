#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "playernumber.h"
#include "information.h"
#include "utils.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void OnControllerConnect();
    void OnControllerDisconnect();
    void SetMsgSender(MsgSender msgSender);

private slots:

    /**
        Passage à la fenêtre PLAYERNUMBER quand le playButton est appuyé

        @param none
        @return void
    */
    void on_playButton_clicked();
    
    
    /**
        Passage à la fenêtre INFORMATION pour l'affichage des règles quand le'infoButton est appuyé

        @param none
        @return void
    */
    void on_infoButton_clicked();
    
    
    /**
        Changer la langue quand languageButton appuyé. Chaque click change l'icon du languageButton

        @param bool checked
        @return void
    */
    void on_languageButton_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    PlayerNumber *window = nullptr;
    Information *rulesWindow = nullptr;
    MsgSender m_msgSender = nullptr;
};

#endif // MAINWINDOW_H
