#ifndef PLAYERNUMBER_H
#define PLAYERNUMBER_H

#include <QDialog>
#include <QGraphicsDropShadowEffect>
#include "online.h"
#include "computer.h"
#include "utils.h"

namespace Ui {
class PlayerNumber;
}

class PlayerNumber : public QDialog
{
    Q_OBJECT

public:
    explicit PlayerNumber(QWidget *parent = 0);
    ~PlayerNumber();

    void OnControllerConnect();
    void OnControllerDisconnect();
    void SetMsgSender(MsgSender msgSender);

private slots:

    /**
        Initialise le nombre des joueurs à 2 quand le bouton est appuyé

        @param bool checked
        @return void
    */
    void on_twoPlayersButton_toggled(bool checked);
    
    
    /**
        Initialise le nombre des joueurs à 3 quand le bouton est appuyé

        @param bool checked
        @return void
    */
    void on_threePlayersButton_toggled(bool checked);
    
    
     /**
        Initialise le nombre des joueurs à 4 quand le bouton est appuyé

        @param bool checked
        @return void
    */
    void on_fourPlayersButton_toggled(bool checked);
    
    
    /**
        Passage à la fenêtre COMPUTER pour commencer le jeu sur l'ordinateur

        @param none
        @return void
    */
    void on_computerButton_clicked();
    
    
    /**
        Passage à la fenêtre ONLINE pour commencer le jeu en ligne

        @param none
        @return void
    */
    void on_onlineButton_clicked();

private:
    Ui::PlayerNumber *ui;
    Computer *windowComputer;
    Online *windowOnline;

    bool m_controllerConnected = false;
    MsgSender m_msgSender = nullptr;
};

#endif // PLAYERNUMBER_H
