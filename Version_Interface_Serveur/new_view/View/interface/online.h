#ifndef ONLINE_H
#define ONLINE_H

#include <QDialog>
#include <QLineEdit>
#include <QGraphicsDropShadowEffect>
#include "game.h"
#include "utils.h"

namespace Ui {
class Online;
}

class Online : public QDialog
{
    Q_OBJECT

public:
    explicit Online(QWidget *parent = 0);
    ~Online();

private slots:

     /**
        Passage à la fenêtre GAME
        Initialise la variable globale name à la valeur donnée dans LineEdit

        @param none
        @return void
    */
    void on_startButton_clicked();
    
    
    /**
        Teste s'il y a de texte dans LineEdit

        @param QString str
        @return void
    */
    void TextChanged(QString str); //text changed

private:
    Ui::Online *ui;
    Game *windowGame;
    QPushButton *startButton;
    QLineEdit *textArea;
};

#endif // ONLINE_H
