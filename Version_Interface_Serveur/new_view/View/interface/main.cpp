#include "mainwindow.h"
#include "View.h"
#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QTranslator>
#include <QLocale>

int main(int argc, char *argv[])
{
    View view(argc, argv);
	
	//QTranslator translator;
	//translator.load("l10n/translations_" + QLocale::system().name());
	//view.installTranslator(&translator);

    if(view.Init({}) == false)
    {
        return 1;
    }

    bool timeToQuit = false;
    while(timeToQuit == false)
    {
        view.Update();
    }
    return 0;
}

