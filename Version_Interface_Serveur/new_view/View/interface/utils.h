#ifndef UTILS_H
#define UTILS_H
#include <QString>
#include <string>
#include "../../Network/IConnection.h"
#include <functional>

using Msg = IConnection::Msg;
using MsgSender = std::function<void(const Msg&)>;

    extern QString name; //utilisé pour le nom de joueur
    extern int nb_players; //nb des joueurs 
    extern std::string language; //utilisé pour choisir la langue

#endif // UTILS_H
